use anyhow::Result;
use std::env;
use std::fs::File;
use std::io;
use std::io::{Read, Write};
#[macro_use]
extern crate log;

use env_logger::Env;
const MEMORY: usize = 30000;
pub const NEWLINE: u8 = 10;
pub const EOF: u8 = 0;
const LOOP_NESTING: usize = 10;

///  Tokens that are valid instructions in Brainfuck.
/// Initially, there was a Token for Other, which I thought would be needed but it is easier to just filter any non valid characters out when the input is read in.
enum Token {
    IncrementPointer(usize),
    DecrementPointer(usize),
    IncrementCurrentByte(usize),
    DecrementCurrentByte(usize),
    ClearCurrentByte,
    WriteCurrentByte,
    ReadCurrentByte,
    BracketStart(usize),
    BracketEnd(usize),
    // Additional Token for merged operations if necessary
}

/// Collapses consecutive similar instructions and optimizes clear loops.
fn collapse_instructions(src_code: &str) -> Vec<Token> {
    let mut program: Vec<Token> = Vec::with_capacity(src_code.len());
    let mut chars = src_code.chars().peekable();

    while let Some(&c) = chars.peek() {
        match c {
            '[' => {
                // Check for the clear loop pattern "[-]"
                if chars.clone().nth(1) == Some('-') && chars.clone().nth(2) == Some(']') {
                    program.push(Token::ClearCurrentByte);
                    chars.next(); // Skip '['
                    chars.next(); // Skip '-'
                    chars.next(); // Skip ']'
                    continue;
                }
                // Otherwise, process normally
                program.push(Token::BracketStart(0));
                chars.next();
            }
            ']' => {
                program.push(Token::BracketEnd(0));
                chars.next();
            }
            _ => {
                let mut count = 0;
                while chars.peek() == Some(&c) {
                    count += 1;
                    chars.next();
                }

                if count > 0 {
                    let token = match c {
                        '>' => Token::IncrementPointer(count),
                        '<' => Token::DecrementPointer(count),
                        '+' => Token::IncrementCurrentByte(count),
                        '-' => Token::DecrementCurrentByte(count),
                        '.' => Token::WriteCurrentByte,
                        ',' => Token::ReadCurrentByte,
                        _ => continue, // Ignore other characters
                    };
                    program.push(token);
                }
            }
        }
    }
    program
}

/// Modified interpretation function to handle clear loops.
fn interpret_code<R: Read, W: Write>(
    src_code: &str,
    mut input: std::io::Bytes<R>,
    output: &mut W,
) -> Result<()> {
    let mut memory = [0_u8; MEMORY];
    let mut pointer = 0;
    let mut instruction_pointer = 0;

    let mut program = collapse_instructions(src_code);

    let _jumps_calculated = calculate_jump_targets(&mut program);

    while instruction_pointer < program.len() {
        match &program[instruction_pointer] {
            Token::IncrementPointer(count) => pointer += *count,
            Token::DecrementPointer(count) => pointer -= *count,
            Token::IncrementCurrentByte(count) => {
                memory[pointer] = memory[pointer].wrapping_add(*count as u8)
            }
            Token::DecrementCurrentByte(count) => {
                memory[pointer] = memory[pointer].wrapping_sub(*count as u8)
            }
            Token::ClearCurrentByte => memory[pointer] = 0,
            Token::WriteCurrentByte => output.write_all(&[memory[pointer]])?,
            Token::ReadCurrentByte => {
                memory[pointer] = match input.next() {
                    Some(Ok(byte)) => byte,
                    _ => EOF,
                };
            }
            Token::BracketStart(target) => {
                if memory[pointer] == 0 {
                    instruction_pointer = *target;
                    continue;
                }
            }
            Token::BracketEnd(target) => {
                if memory[pointer] != 0 {
                    instruction_pointer = *target;
                    continue;
                }
            }
        }
        instruction_pointer += 1;
    }
    output.flush()?;
    Ok(())
}
fn main() -> Result<()> {
    let env = Env::default()
        .filter_or("MY_LOG_LEVEL", "error")
        .write_style_or("MY_LOG_STYLE", "always");

    env_logger::init_from_env(env);

    trace!("some trace log");
    debug!("some debug log");
    info!("some information log");
    warn!("some warning log");
    error!("some error log");

    let src_code = match env::args_os().nth(1) {
        Some(path) => {
            let mut code = String::new();
            let mut input = File::open(path)?;
            input.read_to_string(&mut code)?;
            code
        }
        None => {
            let mut code = String::new();
            let mut input = io::stdin();
            input.read_to_string(&mut code)?;
            code
        }
    };

    let stdin = io::stdin();
    let input = stdin.lock().bytes();
    let stdout = io::stdout();
    let mut output = stdout.lock();

    interpret_code(&src_code, input, &mut output)
}

fn calculate_jump_targets(program: &mut Vec<Token>) -> Result<()> {
    let mut stack: Vec<usize> = Vec::with_capacity(LOOP_NESTING);
    for instruction_index in 0..program.len() {
        match program[instruction_index] {
            Token::BracketStart(_) => {
                stack.push(instruction_index);
            }
            Token::BracketEnd(_) => {
                let stack_index = stack.pop().expect("Mismatched brackets caused this error");
                program[instruction_index] = Token::BracketEnd(stack_index);
                program[stack_index] = Token::BracketStart(instruction_index)
            }
            _ => {}
        }
    }

    if stack.is_empty() {
        Ok(())
    } else {
        panic!("Mismatched Brackets");
    }
}

mod tests {
    #[allow(unused_imports)]
    use super::*;

    #[test]
    fn run_hello_world() {
        let src = include_str!("../sample-programs/hello.bf");
        let input = [].bytes();
        let mut output = Vec::new();

        let result = interpret_code(src, input, &mut output);
        assert!(result.is_ok());

        let output_result = std::str::from_utf8(&output);
        assert!(output_result.is_ok());
        let output_str = output_result.unwrap();

        assert_eq!("Hello World!\n\r", output_str,);
    }

    // we are incrementing the current memory pointers value to 5, then entering a loop that decreases the value located at the memory pointer till it is zero, then exits the loop.
    #[test]
    fn run_increment_and_loop() {
        let src = format!(r#"+++++[-]"#);
        let input = [].bytes();
        let mut output = Vec::new();

        let result = interpret_code(&src, input, &mut output);
        assert!(result.is_ok());

        let output_result = std::str::from_utf8(&output);
        assert!(output_result.is_ok());
        let output_str = output_result.unwrap();

        assert_eq!("", output_str,);
    }

    // This
    #[test]
    fn test_input_collapsing() {
        let src = "++++++++++>>>>>---";
        let collapsed_tokens = collapse_instructions(src);

        // Check the length to ensure it's collapsed
        assert_eq!(
            collapsed_tokens.len(),
            3,
            "Tokens were not collapsed correctly"
        );

        // Check each token to ensure it's the correct type and count
        match collapsed_tokens[0] {
            Token::IncrementCurrentByte(10) => (),
            _ => panic!("First token or count is incorrect"),
        }
        match collapsed_tokens[1] {
            Token::IncrementPointer(5) => (),
            _ => panic!("Second token or count is incorrect"),
        }
        match collapsed_tokens[2] {
            Token::DecrementCurrentByte(3) => (),
            _ => panic!("Third token or count is incorrect"),
        }
    }

    #[test]
    fn test_clear_loop_optimization() {
        // The source code sets the first cell to 3, then clears it with a [-] loop
        let src = "+++[-]";
        let input = [].bytes();
        let mut output = Vec::new();

        // Run the interpreter on the provided source
        let result = interpret_code(&src, input, &mut output);
        assert!(result.is_ok());

        // Ensure that no output is produced
        assert!(
            output.is_empty(),
            "Output should be empty, got: {:?}",
            output
        );

        // Check the memory state
        let memory_snapshot = interpret_memory_snapshot(&src);
        assert_eq!(memory_snapshot[0], 0, "Memory cell should be cleared to 0");

        // Check that the clear loop is optimized
        let tokens = collapse_instructions(src);
        assert!(
            contains_clear_token(&tokens),
            "Optimization for clear loop not applied"
        );
    }

    // Helper function to run the interpreter and return the memory state
    fn interpret_memory_snapshot(src_code: &str) -> [u8; MEMORY] {
        let input = [].bytes();
        let mut output = Vec::new();
        let mut memory = [0_u8; MEMORY];
        interpret_code_with_memory_snapshot(src_code, input, &mut output, &mut memory).unwrap();
        memory
    }

    // Actual interpretation function used to capture memory state
    fn interpret_code_with_memory_snapshot<R: Read, W: Write>(
        src_code: &str,
        mut input: std::io::Bytes<R>,
        output: &mut W,
        memory: &mut [u8; MEMORY],
    ) -> Result<()> {
        let mut pointer = 0;
        let mut instruction_pointer = 0;
        let mut program = collapse_instructions(src_code);
        calculate_jump_targets(&mut program)?;
        while instruction_pointer < program.len() {
            match &program[instruction_pointer] {
                Token::IncrementPointer(count) => pointer += *count,
                Token::DecrementPointer(count) => pointer -= *count,
                Token::IncrementCurrentByte(count) => {
                    memory[pointer] = memory[pointer].wrapping_add(*count as u8)
                }
                Token::DecrementCurrentByte(count) => {
                    memory[pointer] = memory[pointer].wrapping_sub(*count as u8)
                }
                Token::ClearCurrentByte => memory[pointer] = 0,
                Token::WriteCurrentByte => output.write_all(&[memory[pointer]])?,
                Token::ReadCurrentByte => {
                    memory[pointer] = match input.next() {
                        Some(Ok(byte)) => byte,
                        _ => EOF,
                    };
                }
                Token::BracketStart(target) => {
                    if memory[pointer] == 0 {
                        instruction_pointer = *target;
                        continue;
                    }
                }
                Token::BracketEnd(target) => {
                    if memory[pointer] != 0 {
                        instruction_pointer = *target;
                        continue;
                    }
                }
            }
            instruction_pointer += 1;
        }
        output.flush()?;
        Ok(())
    }

    // Check if the program contains a ClearCurrentByte token
    fn contains_clear_token(tokens: &[Token]) -> bool {
        tokens.iter().any(|t| matches!(t, Token::ClearCurrentByte))
    }
}
