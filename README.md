# Pinky : Brainfuck Compiler

## Brainfuck Notes
- Pointer : Moves around an array of 30,000 bytes
- Array : All set to 0 initially
- If the pointer increments one byte above 30k or below 0,, the value is wrapped around
- Weird Characters: Newline is 10, EOF is 0

## Syntax
Eight Operators

| Command | Description |
|-|-|
| `>` | increment pointer |
| `<` | decrement pointer |
| `+` | increment current byte |
| `-` | decrement current byte |
| `.` | write current byte to stdout |
| `,` | read byte from stdin and store value in current byte |
| `[` | jump past matching `]` if current byte is zero |
| `]` | jump back to matching `[` if current byte is nonzero |
| any other char | ignore, treat as comment |

Good brainfuck writeup : https://gist.github.com/roachhd/

## Implementation Details
- Cells are 1 byte
- Variable Memory Size? 30k bytes> 65536 bytes?


## Things I would like to have
- Interpreter
- Compiler
- Optimization passes on the compiler
- Graphviz output for different stages
- Debugger like iamcal.com/misc/bf_debug/
- Visualization Tool like https://fatiherikli.github.io/brainfuck-visualizer/

## Useful Resources
- [Basics Of Brainfuck](https://gist.github.com/roachhd/dce54bec8ba55fb17d3a)
- [Brainwhat](https://github.com/dmitmel/brainwhat)
- [BrainfuckBlogPost](https://github.com/pretzelhammer/rust-blog/blob/master/posts/too-many-brainfuck-compilers.md)
- http://brainfuck.org/
- https://minond.xyz/brainfuck/
- Cool utils http://esoteric.sange.fi/brainfuck/utils/

## Notes
Good example for Description Of Implementation Details : http://brainfuck.org/bcci.c